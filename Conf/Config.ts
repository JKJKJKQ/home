import path from "path";
const entitiesPath = path.join(__dirname,'../','entities');
export default{
    db: {
        host: "localhost",
        type: "mysql",
        username: "amm",
        password: "123456",
        port: 3306,
        database: "test",
        synchronize: true,
        logging: false,
        entities: [
            `${entitiesPath}/**/*`
        ]
    },
    pd:{
        port:8124
    }
};