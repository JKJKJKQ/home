import {MSF} from "@thinkraz/msf";
import Controller = MSF.Controller;
import RpcController = MSF.Decorators.Controller.RpcController;
import RpcPattern = MSF.Decorators.RPC.RpcPattern;
// import {getConnection, getRepository} from "typeorm";
// import User from "../entities/user";
import config from "../Conf/Config"
import UserModule from "../Modules/userModule";

@RpcController(config.pd.port)
export default class MysqlController extends Controller {

    @RpcPattern({service:"mysql",action:"create"})
    async create(args:{table: string, objects: any }){
        await UserModule.createUser(args.table,args.objects);
    }

    @RpcPattern({service:"mysql",action:"remove"})
    async remove(args:{table:string,idArray:Array<any> }){
        await UserModule.removeUser(args.table,args.idArray);
    }

    @RpcPattern({service:"mysql",action:"update"})
    async update(args:{table:string,objects:any,id:number }){
        await UserModule.updateUser(args.table,args.objects,args.id);
    }

    @RpcPattern({service:"mysql",action:"select"})
    async select(args:{table:string,objects:any}){
        await UserModule.selectUser(args.table,args.objects);
    }

}

