import {MSF} from "@thinkraz/msf";
import Application = MSF.Application;
import Program1 from "./Components/Program1";
import MyController from "./Controllers/MyController";
import config from "./Conf/Config"
import TypeOrmComponent = MSF.Components.TypeOrmComponent;
import MysqlController from "./Controllers/MysqlController";
// console.log(config.db);
export default class Server{
    private app : Application;
    constructor(){
        this.app = new Application({
            port: 8080,
            name: "Sort",
            metaComponents: {
                db:{
                    className:TypeOrmComponent,
                    config:config.db
                },
                Program1: {
                    className: Program1,
                    config: {
                        a:[1,2,3,4]
                    }
                }
            },
            metaControllers: [MysqlController]
        });
    }
    run(){
        return new Promise(resolve =>{
            this.app.run(()=>{
                resolve("服务运行车");
            })
        });
    }
};

