import {getConnection, getRepository} from "typeorm";
import User from "../entities/user";
import TableModule from "./tableModule";
import undefinedError = Mocha.utils.undefinedError;

export default class UserModule {
    private static readonly errors = {
        LackParamId:"lack param id",
        LackParamObjects:"lack param objects",
        LackParamTableName:"lack param tableName",
        TableNotFound:"the table is not found",

    };

    static async createUser(tableName: string, objects: any): Promise<any> {
        if(!tableName) throw this.errors.LackParamTableName;
        if(objects.length ===0) throw this.errors.LackParamObjects;
        await TableModule.findTable(tableName).then(async resolve => {
            if (resolve === undefined) {
                await TableModule.createMysql(tableName, objects);
                await TableModule.createTable(tableName);
            } else {
                return true;
            }
        });
        return await getConnection()
            .createQueryBuilder()
            .insert()
            .into(tableName)
            .values(objects)
            .execute()
    };

    static async removeUser(tableName: string,idArray:any):Promise<{lines:number}>{
        if(!idArray) throw this.errors.LackParamId;
        if(!tableName) throw this.errors.LackParamTableName;
        await TableModule.findTable(tableName).then(res=>{
            if(res === undefined){
                throw this.errors.TableNotFound;
            } else{
                return true;
            }
        });
        let data = await getConnection()
            .createQueryBuilder()
            .delete()
            .from(tableName)
            .where(`id in(${idArray})`)
            .execute();
        return {lines:data.raw.affectedRows};
    }


    static async updateUser(tableName: string,objects:any,id:number):Promise<{lines:number}>{
        if(!id) throw this.errors.LackParamId;
        if(!tableName) throw this.errors.LackParamTableName;
        if(objects.length ===0) throw this.errors.LackParamObjects;
        await TableModule.findTable(tableName).then(res=>{
            if(res === undefined){
                throw this.errors.TableNotFound;
            } else{
                return true;
            }
        });
        let data = await getConnection()
            .createQueryBuilder()
            .update(tableName)
            .set(objects)
            .where({id:id})
            .execute();
        await TableModule.updateTable(tableName);
        return {lines:data.raw.affectedRows};
    }


    static async selectUser(tableName: string,objects:any):Promise<any>{
        if(!tableName) throw this.errors.LackParamTableName;
        if(objects.length ===0) throw this.errors.LackParamObjects;
        await TableModule.findTable(tableName).then(res=>{
            if(res === undefined){
                throw this.errors.TableNotFound;
            } else{
                return true;
            }
        });
        let data = await getRepository(tableName)
            .createQueryBuilder("user")
            .where({ id: 1 })
            .getOne();
        console.log(data);
        return data;
    }
}