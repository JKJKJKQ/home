import {getConnection, getRepository} from "typeorm";
import Table from "../entities/table";
import judge from "../Components/judge";

export default class TableModule {
    private static readonly errors= {
        TableNameNotFound: new Error("the table's name is not found"),
        ObjectNotFound: new Error("the object is not found"),
        IdNotFound: new Error("the id is not found"),
    };

    static async findTable(tableName:string) {
        let connection = getConnection().getRepository(Table);
        return connection.findOne({name:tableName});
    }

    static async createMysql(tableName:string,objects:any){
        if (!tableName) throw this.errors.TableNameNotFound;
        let connect = getConnection();
        let messages = await judge(objects);
        await connect.query("create table "+tableName+" (id int auto_increment primary key,"+messages+")");

    }

    static async createTable(tableName:string,):Promise<any> {
        let connection = getConnection().getRepository(Table);
        let table = new Table();
        table.name = tableName;
        table.version = 1;
        table.create_at = Date.now();
        table.update_at = Date.now();
        return connection.save(table)
    }

    static async updateTable(tableName:string):Promise<any> {
        let connection = getConnection().getRepository(Table);
        let table = await connection.findOne({name:tableName});
        table.version ++;
        table.update_at = Date.now();
        return connection.save(table)
    }

}