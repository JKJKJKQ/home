import Server from "./APP";
import {MSF} from "@thinkraz/msf";
import {RPC} from "@thinkraz/rpc";


(new Server()).run().then(async ()=>{
    console.log('then后面的试运作');
    let client = new RPC.Client('127.0.0.1',8124,false);
    let r = await client.call({
        service: "mysql",
        action: "select",
        table: "OOO2",
        objects: {
            name:"nihao",
            single:true,
            favoriteFood:"chicken"
        }
    })
}).catch(err=>{
    console.log(err);
});