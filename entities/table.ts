import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export default class Table {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    version: number;

    @Column({type: "bigint"})
    create_at: number;


    @Column({type: "bigint"})
    update_at: number;

}